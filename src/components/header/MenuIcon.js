import React, {Component}  from "react";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {DrawerActions} from "react-navigation-drawer";

export default class MenuIcon extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <Icon
        style={{marginRight : 15,}}
        name="menu"
        color="white"
        size={36}
        underlayColor="#4BA6F8"
        onPress={() => {
          const openDrawerAction = DrawerActions.toggleDrawer();
          navigation.dispatch(openDrawerAction);
        }}
      />
    );
  }
}

