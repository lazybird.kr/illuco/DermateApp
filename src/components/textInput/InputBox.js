import React,{ Component } from 'react';
import { Keyboard, View, TextInput, Button, StyleSheet, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

export default class InputBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
        keyboardHeight: 0,
        inputHeight: 50
    }
}
  componentDidMount() {
    Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
    Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
  }

  _keyboardDidShow(e) {
      this.setState({keyboardHeight: e.endCoordinates.height});
  }

  _keyboardDidHide(e) { 
      this.setState({keyboardHeight: 0});
  }

  render() {
    const {onChangeText, onSubmitEditing, value} = this.props;
    const isValue = value && value.length > 0 ? true : false; 
    const {keyboardHeight, inputHeight} = this.state;
    
    return(
      <View style={{
                    marginBottom : Platform.OS === "ios" ? keyboardHeight : 0,
                    flexDirection:"row", backgroundColor : "#FFFFFF",}}>
        <TextInput
          multiline={true}
          autoFocus={true}
          onChangeText={onChangeText}
          onContentSizeChange={(event) => {
            this.setState({ inputHeight: event.nativeEvent.contentSize.height })
          }}
          style={{...styles.text, ...{height : Math.max(35, this.state.inputHeight)}}}
          value={value}
          autoCorrect={false}
        />
        <View style={{flex:1}}></View>
        {isValue && <Icon style={styles.button} 
              name="arrow-up-circle" size={28} 
              onPress={onSubmitEditing} />}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  text: {
      fontFamily: 'System',
      fontStyle: 'normal',
      fontSize: 16,
      lineHeight: 20,
      letterSpacing: 0.6,
      paddingLeft : 10,
      flex : 8,      
      justifyContent : "center",
      alignSelf : "center",
      alignItems : "center"
  },
  button : {
    flex : 1,
    alignSelf : "center",
    justifyContent : "flex-end",
    alignItems : "flex-end",
    paddingLeft : 10,
    
  }
})