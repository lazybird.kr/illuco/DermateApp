import ConfirmDialog from './ConfirmDialog';
import ConfirmOkDialog from './ConfirmOkDialog';
import PatientInputModal from './PatientInputModal';

export {
  ConfirmDialog,
  ConfirmOkDialog,
  PatientInputModal,
}