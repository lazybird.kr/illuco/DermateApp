import React, {Component}  from "react";
import {View, Text, FlatList,Keyboard, StatusBar, Platform} from "react-native";
import config from 'src/config';
import {PatientItem} from 'src/components/patient';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { SearchBar } from 'react-native-elements';


export default class PatientInputModal extends Component {
    constructor() {
        super();
        this.state = {
            searchValue : '',
            searchData : [],
            keyboardHeight: 0,
        }
        this._isMounted = false;
    }

    searchFilterFunction = text => {
        const { searchResult,} = this.props;
        if(this._isMounted)
          this.setState({ searchValue : text });
    
        if(!searchResult.patient_list)
          return;
    
        const newData = searchResult.patient_list.filter(item => {
          const itemData = `${item.name.toUpperCase()}`;
          const textData = text.toUpperCase();
    
          return itemData.indexOf(textData) > -1;
        });
        if(this._isMounted)
          this.setState({searchData : newData});
      }
      searchHeader = () => {
        const { searchValue } = this.state;
        return (
          <SearchBar  
                  ref={search => this.search = search}
                  platform={Platform.OS === "ios" ? "ios" : "android"}
                  cancelButtonTitle="Cancel"
                  inputStyle={{height : 40}} 
                  containerStyle={{backgroundColor:"#FBFBFB",
                    borderBottomColor: 'transparent', borderTopColor: 'transparent'}} 
                  placeholder="Search for Patients"
                  onChangeText={this.searchFilterFunction}
                  onSubmitEditing ={this.handleSearch}
                  onFocus={this.handleFocus}
                  onBlur={this.bandleBlur}
                  onCancel={this.handleCancel}
                  onClear={this.handleClear}
                  onClearText={this.handleClearText}  
                  onEndEditing={this.handleEndEditing}
                  autoCorrect={false}
                  value={searchValue}
                />
        );
      }
      
      componentDidMount() {
        this._isMounted = true;
        this.setState({searchData : this.props.searchResult.patient_list});
        this.subs = [
          Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this)),
          Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this)),
        ];
      }

      componentWillUnmount() {
        this.subs.forEach((sub) => {
          sub.remove();
        });
        this._isMounted = false;
      }
    
      _keyboardDidShow(e) {
        if(this._isMounted)
          this.setState({keyboardHeight: e.endCoordinates.height});
      }
    
      _keyboardDidHide(e) {
        if(this._isMounted) 
          this.setState({keyboardHeight: 0});
      }
      
      renderSeparator = () => {
        return (
          <View
            style={{
              height: 1,
              width: '86%',
              backgroundColor: '#CED0CE',
              marginLeft: '14%',
            }}
          />
        );
      }
      handleFocus = () => {
          this.setState({isPatientListViable : true})
      }
    
      handleCancel = () => {   
          this.setState({
            searchValue : "", isPatientListViable : false,
          });
      }
    
      handleClear = () => {   
          this.setState({
            searchValue : "", isPatientListViable : false,
          });
      }
    
      handleClearText = () => {
        this.setState({
          searchValue : "", isPatientListViable : false,
        });
      }
    
      handleBlur = () => {
        this.setState({
          searchValue : "", isPatientListViable : false,
        });
      }
    
      handleEndEditing = () => {   
        this.setState({
          searchValue : "", isPatientListViable : false,
        });
      }

      render() {
          return(
          
              <View style={{ width:"100%", 
                            height : this.state.keyboardHeight? 
                                config.styleConstants.height - this.state.keyboardHeight - (Platform.OS === 'ios' ? 20 : StatusBar.currentHeight)
                                : config.styleConstants.height * 0.7,}}>
                  <View style={{
                      borderTopLeftRadius : 20,
                      borderTopRightRadius : 20,
                      alignItems: 'center',
                      height: 60,
                      borderBottomWidth : 1,
                      borderColor : "grey",
                      justifyContent : "center",
                      flexDirection : "row",
                      paddingLeft : 10,
                      paddingRight : 10,
                      backgroundColor : "#FFFFFF"
                    }}>
                      <View
                          style={{flex : 1}}
                      />
                      <View style={{flex : 8, justifyContent:"center", alignItems : "center"}}>
                        <Text style={{fontSize: 16}}>Patient</Text>
                      </View>
                      <Icon
                        name="close"
                        size={20}
                        color='#525966'
                        style={{ flex : 1, marginLeft: 10 }}
                        onPress={this.props.togglePatient}
                      />
                    </View>
                    <FlatList 
                      style={{backgroundColor:"#FFFFFF"}}
                      data={this.state.searchData}
                      renderItem={({ item }) => (
                        <PatientItem item={item} handleClick={this.props.handleSelectPatient}/>)}
                      keyExtractor={item => item.patient_id}
                      ItemSeparatorComponent={this.renderSeparator}
                      ListHeaderComponent={this.searchHeader}
                      keyboardShouldPersistTaps="always"
                      stickyHeaderIndices={[0]}
                      //contentContainerStyle={{ paddingBottom: 50}}
                    />
                </View>
          )
      }

     
}