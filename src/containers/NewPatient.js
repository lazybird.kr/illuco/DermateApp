import React, {Component}  from "react";
import {View, Text, StyleSheet, Image, TouchableOpacity,ScrollView, TextInput, Alert,ActivityIndicator, Platform, KeyboardAvoidingView } from "react-native";
import {BackIcon, MenuIcon, HeaderTitle} from 'src/components/header';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import config from 'src/config';
import {CheckBox} from 'react-native-elements';
import DateTimePicker from 'react-native-datepicker';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {Appearance} from 'react-native-appearance'
import { ActionSheet,Root } from "native-base";
import ImagePicker from 'react-native-image-crop-picker';
import Modal from "react-native-modal";
import { DermSingleSelect } from "src/components/select";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as detailActions from 'src/modules/detail';

const colorScheme = Appearance.getColorScheme();

class NewPatient extends Component {  
  constructor() {
    super();
  }

  state = {
    genderFemaleChecked: true,
    genderMaleChecked: false,

    skinCancerHistoryYes : false,
    skinCancerHistoryNo : true,

    skinFamilyCancerHistoryYes : false,
    skinFamilyCancerHistoryNo : true,


    birth_date : null,
    pid : '',
    firstName : '',
    lastName : '',
    gender : 'F',
    ethnicity : '',
    skin_type : '',
    skin_cancer_history : '',        /* Y or N */
    family_skin_cancer_history : '',   /* Y or N */
    country : '',
    phone : '',
    description : '',
    
    picture : null,

    hospital_id : '',
    user_id : '',
    
    loading : false,
    isEditable : false,
    isSaveConfirm : false,


    singleEthnicityModalOpen : false,
    ethnicityItems : [],
    selectedEthnicity : [],

    singleCountryModalOpen : false,
    countryItems : [],
    selectedCountry : [],

    singleSkinTypeModalOpen : false,
    skinTypeItems : [],
    selectedSkinType : [],

  };

  onTakePic = data => {
    this.setState({picture : data});
  };

  onSelectedImage = (image) => {
    this.setState({
      picture : {
        uri: image.path,
        width : image.width,
        height : image.height,
        mime : image.mime
      }
    })
  }

  takePhotoFromCamare = () => {
    ImagePicker.openCamera({
      compressImageMaxHeight:1920,
      compressImageMaxWidth:1920,
      compressImageQuality : 1.0,
      cropping:false,
    }).then(image => {
      console.log(image);
      ImagePicker.openCropper({
        path: image.path,
        height:image.height,
        width:image.width,
        compressImageQuality: 1.0,
        cropperCircleOverlay: true,
        hideBottomControls: true,
        freeStyleCropEnabled : false,
        cropperStatusBarColor: config.colors.headerColor,
        cropperToolbarColor: config.colors.headerColor,
      }).then(data => {
        this.onSelectedImage(data);
      }).catch(err => {
        console.log(err);
      })
    }).catch(err => {
      console.log(err);
    })
  }

  choosePhotoFromLibrary = () => {
    ImagePicker.openPicker({
      compressImageMaxHeight:1920,
      compressImageMaxWidth:1920,
      compressImageQuality : 1.0,
      cropping:false,
    }).then(image => {
      console.log(image);
      ImagePicker.openCropper({
        path: image.path,
        height:image.height,
        width:image.width,
        compressImageQuality: 1.0,
        cropperCircleOverlay: true,
        hideBottomControls: true,
        freeStyleCropEnabled : false,
        cropperStatusBarColor: config.colors.headerColor,
        cropperToolbarColor: config.colors.headerColor,
      }).then(data => {
        console.log(data);
        this.onSelectedImage(data);
      }).catch(err => {
        console.log(err);
      })
    }).catch(err => {
      console.log(err);
    })
  }

  clickAddImage = () => {
    console.log("AAA");
    const BUTTONS = ['Take Photo', 'Choose Photo Library', 'Cancel'];
    ActionSheet.show({options:BUTTONS, cancelButtonIndex:2, title :'Select a Photo'},
                      buttonIndex => {
                        switch (buttonIndex){
                          case 0:
                            this.takePhotoFromCamare();
                            break;
                          case 1:
                            this.choosePhotoFromLibrary();
                            break;
                          default:
                            break;
                        }
                      });
  }

  static navigationOptions = ({ navigation }) => {
    const image = config.images.patientIcon;
    return {
      headerLeft: <BackIcon navigation={navigation}/>,
      headerTitle : <HeaderTitle image={image} title={navigation.state.params.title} />,
      headerRight : <MenuIcon navigation={navigation}/>,
      headerStyle : {
        backgroundColor: config.colors.headerColor,
      },
    }
  }

  setGender = (gender) => {
    switch (gender) {
      case 'M':
        this.setState({
          genderFemaleChecked : false,
          genderMaleChecked : true,
          gender : 'M',
        })
        break;
      case 'F':
        this.setState({
          genderFemaleChecked : true,
          genderMaleChecked : false,
          gender : 'F',
        })
        break;
        }
  }

  setSkinCancerHistory = (cancer) => {
    switch (cancer[0]) {
      case 'Y':
        this.setState({
          skinCancerHistoryYes : true,
          skinCancerHistoryNo : false,
          skin_cancer_history : 'Y',
        })
        break;
      case 'N':
        this.setState({
          skinCancerHistoryNo : true,
          skinCancerHistoryYes : false,
          skin_cancer_history : 'N',
        })
        break;
    }
  }

  setFamilySkinCancerHistory = (cancer) => {
    switch (cancer[0]) {
      case 'Y':
        this.setState({
          skinFamilyCancerHistoryYes : true,
          skinFamilyCancerHistoryNo : false,
          family_skin_cancer_history : 'Y',
        })
        break;
      case 'N':
        this.setState({
          skinFamilyCancerHistoryNo : true,
          skinFamilyCancerHistoryYes : false,
          family_skin_cancer_history : 'N',
        })
        break;
    }
  }

  checkInput = () => {
    const {birth_date, gender, firstName, lastName, pid} = this.state;

    if (birth_date == null) {
      alert("Please Enter Birth Day");
      return false;
    } else if(firstName.length == 0){
      alert("Please Enter First Name");
      return false;
    } else if(lastName.length == 0){
      alert("Please Enter Last Name");
      return false;
    }
    return true;
  }

  confirm = () => {
    if(this.checkInput()){
      
      if(this.state.isEditable){
        this.handleSubmitEdit();
      } else {
        this.handleSubmit();
      }
      
      //Alert.alert("Confirm", JSON.stringify(this.state));
    } else{
      
    }
  }

  handleSubmit = async () => {
    const {birth_date, gender, firstName, lastName, pid, 
          ethnicity, skin_type, skin_cancer_history,
          family_skin_cancer_history, country, phone, description,
          access_info} = this.state;
    try {
      const send_data = {
          "category": "patient",
          "service": "AddPatient",
          "access_token": access_info.access_token,
          "patient": {
              pid : pid,
              first_name : firstName,
              last_name : lastName,
              birth_date : birth_date,
              gender : gender,
              ethnicity,
              skin_type,
              skin_cancer_history,
              family_skin_cancer_history,
              country,
              phone,
              description
          }
      };
      this.setState({loading : true})
      const response = await axios.post(config.baseUrl + '/api/', 
        {"data" : send_data});
      console.log("AddPatient : ", response.data.data);
      setTimeout(() => {this.setState({loading: false})}, 1500)
      if(!response.data.err){
        this.props.navigation.goBack();
      }
    } catch (err) {
      console.log("AddPatient err : ", err)
    }
  }

  handleSubmitEdit = async () => {
    const {birth_date, gender, firstName, lastName, pid, 
          hospital_id, user_id,  patient_id,
          ethnicity,
          skin_type,
          skin_cancer_history,
          family_skin_cancer_history,
          country,
          phone,
          description,
          access_info} = this.state;
    try {
      const send_data = {
          "category": "patient",
          "service": "UpdatePatient",
          "access_token": access_info.access_token,
          "patient": {
              patient_id : patient_id,
              hospital_id : hospital_id,
              user_id : user_id,
              pid : pid,
              first_name : firstName,
              last_name : lastName,
              birth_date : birth_date,
              gender : gender,
              ethnicity : ethnicity,
              skin_type : skin_type,
              skin_cancer_history : skin_cancer_history,
              family_skin_cancer_history : family_skin_cancer_history,
              country : country,
              phone : phone,
              description : description,
          }
      };
      this.setState({loading : true})
      const response = await axios.post(config.baseUrl + '/api/', 
        {"data" : send_data});
      console.log("UpdatePatient : ", response.data);
      setTimeout(() => {this.setState({loading: false})}, 1500)
      if(!response.data.err){
        this.props.navigation.goBack();
      }
    } catch (err) {
      console.log("UpdatePatient err : ", err)
    }
  }

  componentDidMount() {
    console.log("New Patient componentDidMount()");

    AsyncStorage.getItem('access_info')
    .then(value => { 
      const access_info = JSON.parse(value);
      if(this.props.navigation.state.params && this.props.navigation.state.params.patient){
        this.props.DetailActions.getDetailData(access_info.access_token, this.props.navigation.state.params.patient.patient_id, 'patient'); 
      }
      this.setState({access_info : access_info});
      this.props.DetailActions.getItemData(access_info.access_token, 'ethnicity');
      this.props.DetailActions.getItemData(access_info.access_token, 'country');
      this.props.DetailActions.getItemData(access_info.access_token, 'skin_type');

      this.subs = [
        this.props.navigation.addListener(
          "didFocus", () => {console.log("didFocus"); this.setState({isFocused: true});}),
        this.props.navigation.addListener("willBlur", () => {console.log("willBlur"); this.setState({ isFocused: false, });}),
        this.props.navigation.addListener("willFocus", () => console.log("willFocus")),
        this.props.navigation.addListener("didBlur", () => console.log("didBlur")),
      ];
    });

    /* edit patient 이면 state를 채운다. edit가 아니면 그냥 new patient*/
    
  }
  componentWillUnmount() {
    console.log("New Patient componentWillUnmount()");
    this.subs.forEach((sub) => {
      sub.remove();
    });
  }

  componentDidUpdate(prevProps, prevStates) {

    if(this.props.navigation.state.params && this.props.navigation.state.params.patient) {
      if(JSON.stringify(prevProps.patient) !== JSON.stringify(this.props.patient)){
        const {patient_id, pid, last_name, first_name, birth_date, gender, 
          ethnicity, skin_type, skin_cancer_history, family_skin_cancer_history, 
          country, phone, description, hospital_id, user_id,
        } = this.props.patient;

        console.log("this.props.patient : ", this.props.patient);
        this.setState({
          patient_id : patient_id,
          pid : pid,
          lastName : last_name,
          firstName : first_name,
          birth_date : birth_date,
          gender : gender,
          ethnicity : ethnicity,
          skin_type : skin_type,
          skin_cancer_history : skin_cancer_history,       /* Y or N */
          family_skin_cancer_history : family_skin_cancer_history,   /* Y or N */
          country : country,
          phone : phone,
          description : description,
          hospital_id : hospital_id,
          user_id : user_id,
    
          isEditable : true,
        })

        this.setGender(gender? gender : 'F');
        this.setSkinCancerHistory(skin_cancer_history? skin_cancer_history : 'N');
        this.setFamilySkinCancerHistory(family_skin_cancer_history? family_skin_cancer_history : 'N');
        this.setEthnicity(ethnicity);
        this.setCountry(country);
        this.setSkinType(skin_type);
      }
    }
    if(prevStates.isFocused !== this.state.isFocused && this.state.isFocused){
      this.setState({ethnicityItems : this.props.itemList.ethnicity_item_list,
        skinTypeItems : this.props.itemList.skin_type_item_list,
        countryItems : this.props.itemList.country_item_list,
      });
    }
  }
  handleClickCancelButton = () => {
    this.setState({
      isEditable : false,
    });
    this.props.navigation.goBack();
  }
  handleClickSaveButton = () => {
    this.setState({isSaveConfirm : false});
  }


  singleEthnicityToggleModal = () =>{
    this.setState({singleEthnicityModalOpen : !this.state.singleEthnicityModalOpen});
  }
  onAddEthnicityItem = items => {
    console.log("onAddTypeItem : ", items);
    let newItems = [];
    items.forEach((i) => {
      newItems.push(i.name);
    })
    this.setState({ ethnicityItems : newItems });
  }
  onSelectedEthnicityChange = selectedItem => {
    console.log("onSelectedTypeChange : ", selectedItem);
    this.setState({ selectedEthnicity : selectedItem,
                    ethnicity : selectedItem[0],
    });
  }
  setEthnicity = item => {
    let newItems = [];
    newItems.push(item);
    this.setState({ selectedEthnicity : newItems });
  }

  singleCountryToggleModal = () =>{
    this.setState({singleCountryModalOpen : !this.state.singleCountryModalOpen});
  }
  onAddCountryItem = items => {
    console.log("onAddTypeItem : ", items);
    let newItems = [];
    items.forEach((i) => {
      newItems.push(i.name);
    })
    this.setState({ countryItems : newItems });
  }
  onSelectedCountryChange = selectedItem => {
    console.log("onSelectedTypeChange : ", selectedItem);
    this.setState({ selectedCountry : selectedItem,
                    country : selectedItem[0],
    });
  }
  setCountry = item => {
    let newItems = [];
    newItems.push(item);
    this.setState({ selectedCountry : newItems });
  }

  singleSkinTypeToggleModal = () =>{
    this.setState({singleSkinTypeModalOpen : !this.state.singleSkinTypeModalOpen});
  }
  onAddSkinTypeItem = items => {
    console.log("onAddTypeItem : ", items);
    let newItems = [];
    items.forEach((i) => {
      newItems.push(i.name);
    })
    this.setState({ skinTypeItems : newItems });
  }
  onSelectedSkinTypeChange = selectedItem => {
    console.log("onSelectedTypeChange : ", selectedItem);
    this.setState({ selectedSkinType : selectedItem,
                    skin_type : selectedItem[0],
    });
  }
  setSkinType = item => {
    let newItems = [];
    newItems.push(item);
    this.setState({ selectedSkinType : newItems });
  }

  render() {
    const {birth_date, pid, firstName, lastName, ethnicityItems, selectedEthnicity,
          phone, description, singleEthnicityModalOpen,
          countryItems, selectedCountry, singleCountryModalOpen,
          skinTypeItems, selectedSkinType, singleSkinTypeModalOpen,
          loading, isEditable} = this.state;
    
    const {onSelectedEthnicityChange, singleEthnicityToggleModal, onAddEthnicityItem} = this;
    const {onSelectedCountryChange, singleCountryToggleModal, onAddCountryItem} = this;
    const {onSelectedSkinTypeChange, singleSkinTypeToggleModal, onAddSkinTypeItem} = this;


    return (
      <Root style={styles.container}>
        {loading &&
           <View style={styles.loading}>
              <ActivityIndicator size='large' />
            </View>}

        <Modal isVisible={singleEthnicityModalOpen}
              onBackdropPress={singleEthnicityToggleModal}
              avoidKeyboard={true}
              style={styles.modalStyle} >
          <DermSingleSelect items={ethnicityItems} 
            selectedItems={selectedEthnicity}
            style={{flex:1}}
            onSelectedItemsChange={onSelectedEthnicityChange}
            onAddItem={onAddEthnicityItem}
            onToggleList={singleEthnicityToggleModal}
            onCloseList={singleEthnicityToggleModal}
            title="Ethnicity" /> 
        </Modal>
        <Modal isVisible={singleCountryModalOpen}
              onBackdropPress={singleCountryToggleModal}
              avoidKeyboard={true}
              style={styles.modalStyle} >
          <DermSingleSelect items={countryItems} 
            selectedItems={selectedCountry}
            style={{flex:1}}
            onSelectedItemsChange={onSelectedCountryChange}
            onAddItem={onAddCountryItem}
            onToggleList={singleCountryToggleModal}
            onCloseList={singleCountryToggleModal}
            title="Country" /> 
        </Modal>

        <Modal isVisible={singleSkinTypeModalOpen}
              onBackdropPress={singleSkinTypeToggleModal}
              avoidKeyboard={true}
              style={styles.modalStyle} >
          <DermSingleSelect items={skinTypeItems} 
            selectedItems={selectedSkinType}
            style={{flex:1}}
            onSelectedItemsChange={onSelectedSkinTypeChange}
            onAddItem={onAddSkinTypeItem}
            onToggleList={singleSkinTypeToggleModal}
            onCloseList={singleSkinTypeToggleModal}
            title="Skin Type" /> 
        </Modal>


        <View style={styles.body}>
          <KeyboardAwareScrollView contentContainerStyle={{flexGrow: 1}}
            resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={true}>
            <View style={styles.patientImage}>
              <View style={{flex : 1, alignItems:"center"}}>
              {this.state.picture? 
                <Image style={styles.patientPic} source={{uri:this.state.picture.uri}} />
                :
                <Image style={styles.patientEmptyPic} source={config.images.emptyPictureImage} />}
              </View>
              <View style={styles.patientPicButton}>
                <TouchableOpacity onPress={this.clickAddImage}>
                  <Image source={config.images.quickSnapIcon} style={{width:50, height:50}}/>
                </TouchableOpacity>
              </View>
            </View>
            
            <View style={styles.patientItem}>
              <Text style={styles.textInputTitle}>PID</Text>
              <TextInput style={styles.textInput} 
                onChangeText={(text) => {this.setState({pid: text})}} 
                value={pid}
                maxLength={8}
                autoCorrect={false} />
            </View>
            <View style={styles.patientItem}>
              <Text style={styles.textInputTitle}>First Name</Text>
              <TextInput style={styles.textInput} 
                onChangeText={(text) => {this.setState({firstName: text})}} 
                value={firstName}
                autoCorrect={false} />
            </View>
            <View style={styles.patientItem}>
              <Text style={styles.textInputTitle}>Last Name</Text>
              <TextInput style={styles.textInput} 
                onChangeText={(text) => {this.setState({lastName: text})}} 
                value={lastName}
                autoCorrect={false} />
            </View>
            <View style={styles.patientItem}>
              <Text style={styles.textInputTitle}>Birthday</Text>
              <DateTimePicker
                style={{flex: 7}}
                date={birth_date} mode="date"
                androidMode="default"
                format="YYYY/MM/DD" minDate="1910/01/01"
                confirmBtnText="Confirm" cancelBtnText="Cancel"
                customStyles={{
                  datePicker: {backgroundColor:colorScheme === "dark" ? "#222" : "white" },
                  datePickerCon: { backgroundColor: colorScheme === 'dark' ? '#333' : 'white'},
                  dateIcon: datePickerStyles.datePickerDateIcon,
                  dateInput: datePickerStyles.datePickerDateInput,
                  dateText: datePickerStyles.datePickerDateText,
                  placeholderText: datePickerStyles.datePickerPlaceholderText
                }}
                onDateChange={(date) => {this.setState({birth_date: date})}}
              />
            </View>
            <View style={{...styles.patientItem}}>
              <Text style={styles.textInputTitle}>Gender</Text>
              <CheckBox
                textStyle={{fontWeight:"normal", fontSize:16, color:"black"}}
                containerStyle={{flex: 3, justifyContent:"center", backgroundColor:"white", borderColor : "white",}}
                title='Female'
                checked={this.state.genderFemaleChecked}
                onPress={()=>{this.setGender('F')}}
              />
              <CheckBox
                textStyle={{fontWeight:"normal", fontSize:16, color:"black"}}
                containerStyle={{flex: 3, justifyContent:"center", backgroundColor:"white", borderColor : "white",}}
                title='Male'
                checked={this.state.genderMaleChecked}
                onPress={()=>{this.setGender('M')}}
              />
              
            </View>

            <View style={{...styles.patientItem}}>
              <Text style={styles.textInputTitle}>Ethnicity</Text>
              <View style={{flex:6, justifyContent:"center"}}>
                <Text style={{...styles.itemTextStyle, ...{paddingLeft: 5, color : selectedEthnicity && selectedEthnicity[0] ? "black" : "grey" }}}>
                {selectedEthnicity && selectedEthnicity[0]? selectedEthnicity[0] : "Choose"}</Text>
              </View>
              <View style={{flex:1, justifyContent:"center"}}>
                <Icon onPress={singleEthnicityToggleModal} name="chevron-down" size={28} color="grey"/>
              </View>
            </View>
            <View style={{...styles.patientItem}}>
              <Text style={styles.textInputTitle}>Country</Text>
              <View style={{flex:6, justifyContent:"center"}}>
                <Text style={{...styles.itemTextStyle, ...{paddingLeft: 5, color : selectedCountry && selectedCountry[0] ? "black" : "grey" }}}>
                {selectedCountry && selectedCountry[0]? selectedCountry[0] : "Choose"}</Text>
              </View>
              <View style={{flex:1, justifyContent:"center"}}>
                <Icon onPress={singleCountryToggleModal} name="chevron-down" size={28} color="grey"/>
              </View>
            </View>
            <View style={{...styles.patientItem}}>
              <Text style={styles.textInputTitle}>Skin Type</Text>
              <View style={{flex:6, justifyContent:"center"}}>
                <Text style={{...styles.itemTextStyle, ...{paddingLeft: 5, color : selectedSkinType && selectedSkinType[0] ? "black" : "grey" }}}>
                {selectedSkinType && selectedSkinType[0]? selectedSkinType[0] : "Choose"}</Text>
              </View>
              <View style={{flex:1, justifyContent:"center"}}>
                <Icon onPress={singleSkinTypeToggleModal} name="chevron-down" size={28} color="grey"/>
              </View>
            </View>
            
            


            <View style={{...styles.patientItem}}>
              <Text style={styles.textInputTitle}>Skin Cancer Hx</Text>
              <CheckBox
                textStyle={{fontWeight:"normal", fontSize:16, color:"black"}}
                containerStyle={{flex: 3, justifyContent:"center", backgroundColor:"white", borderColor : "white",}}
                title='Yes'
                checked={this.state.skinCancerHistoryYes}
                onPress={()=>{this.setSkinCancerHistory('Y')}}
              />
              <CheckBox
                textStyle={{fontWeight:"normal", fontSize:16, color:"black"}}
                containerStyle={{flex: 3, justifyContent:"center", backgroundColor:"white", borderColor : "white",}}
                title='No'
                checked={this.state.skinCancerHistoryNo}
                onPress={()=>{this.setSkinCancerHistory('N')}}
              />
            </View>
            <View style={{...styles.patientItem}}>
              <Text style={styles.textInputTitle}>Family Hx</Text>
              <CheckBox
                textStyle={{fontWeight:"normal", fontSize:16, color:"black"}}
                containerStyle={{flex: 3, justifyContent:"center", backgroundColor:"white", borderColor : "white",}}
                title='Yes'
                checked={this.state.skinFamilyCancerHistoryYes}
                onPress={()=>{this.setFamilySkinCancerHistory('Y')}}
              />
              <CheckBox
                textStyle={{fontWeight:"normal", fontSize:16, color:"black"}}
                containerStyle={{flex: 3, justifyContent:"center", backgroundColor:"white", borderColor : "white",}}
                title='No'
                checked={this.state.skinFamilyCancerHistoryNo}
                onPress={()=>{this.setFamilySkinCancerHistory('N')}}
              />
            </View>
            <View style={styles.patientItem}>
              <Text style={styles.textInputTitle}>Phone</Text>
              <TextInput style={styles.textInput} 
                onChangeText={(text) => {this.setState({phone: text})}} 
                value={phone}
                autoCorrect={false} />
            </View>
            <View style={styles.patientItem}>
              <Text style={styles.textInputTitle}>Desc</Text>
              <TextInput style={styles.textInput} 
                onChangeText={(text) => {this.setState({description: text})}} 
                value={description}
                autoCorrect={false} />
            </View>
            <View style={{...styles.patientItem, ...{borderWidth:0}}}></View>
          
          </KeyboardAwareScrollView>
          
        </View >
        {isEditable ? 
           <View style={styles.bottomSave}>
           <View style={styles.bottomSaveSub1}>
             <TouchableOpacity style={styles.cancelButton} onPress={this.handleClickCancelButton}>
               <Text style={styles.cancelButtonText}>Cancel</Text>
             </TouchableOpacity>
           </View>
           <View style={styles.bottomSaveSub1}>
             <TouchableOpacity style={styles.saveButton} onPress={this.confirm}>
                 <Text style={styles.saveButtonText}>Save</Text>
             </TouchableOpacity>
           </View>
         </View>
        :
          <View style={styles.bottom}>
            <TouchableOpacity style={styles.confirmButton} onPress={this.confirm}>
              <Text style={styles.confirmButtonText}>Confirm</Text>
            </TouchableOpacity>
          </View>
          }
    </Root>
    );
  }
}

export default connect(
  (state) => ({
      patient : state.detail.get('patientDetail').toJS(),
      itemList : state.detail.get('itemList').toJS(),
  }),
  (dispatch) => ({
      DetailActions:bindActionCreators(detailActions, dispatch),
  })
)(NewPatient);



const styles = StyleSheet.create({
  container : {
    height: "100%", 
    flex: 1,
  },
  body:{
    flex: 8,
  },
  patientImage : {
    flexDirection: "column",
    flex: 2,
    borderBottomColor: config.colors.textborderBottomColor,
    //borderBottomWidth : 1,
    marginBottom : 10
    
  },
  patientEmptyPic: {
    marginTop : 10,
    marginBottom : 10,
    resizeMode: 'contain',
    width: config.styleConstants.oneThirdWidth,
    height: config.styleConstants.oneThirdWidth,
    
  },
  patientPic: {
    marginTop : 10,
    marginBottom : 10,
    width: config.styleConstants.oneThirdWidth,
    height : config.styleConstants.oneThirdWidth,
    borderRadius: 100,
    
  },
  patientPicButton : {
    flex: 0.5, 
    justifyContent: 'center', 
    alignItems: "flex-end", 
    marginRight: 25, 
    //marginTop: -25,
    bottom : 10,
    right : 0,
    position : 'absolute',
    
  },
  patientItem : {
    //flex : 1,
    height : 60,
    marginLeft: 20,
    marginRight: 20,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderColor: config.colors.textborderBottomColor,
    borderWidth: 1,
    marginBottom : 10
  },
  textInputTitle :{
    flex:3, fontSize : 16, textAlignVertical: "center",
    color: "#354d67", marginLeft: 10,
  },
  textInput :{
    textAlignVertical: "center",
    flex:7, fontSize : 16,
  },
  itemTextStyle : {
    fontSize: 16,
    color: "#354d67"
  },
  modalStyle: { 
    flex: 1,
    justifyContent: "flex-end",
    margin: 0
  },
  bottom : {
    flex : 1,
    backgroundColor : config.colors.confirmBackGroundColor,
  },
  confirmButton : {
    width : "95%",
    backgroundColor: config.colors.confirmButtonColor,
    padding: 10,
    margin : 10,
    borderRadius: 5,
    justifyContent : "center",
    alignItems : "center",
  },
  confirmButtonText : {
    alignItems : "center",
    color : "white",
    textAlign : "center",
    fontSize : 18 
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  bottomSave:{
    flex:1,
    backgroundColor: config.colors.confirmBackGroundColor,
    flexDirection : "row"
  },
  bottomSaveSub1 :{
    flex:1,
    //justifyContent : "center",
  },
  cancelButton : {
    width : "95%",
    backgroundColor: config.colors.confirmButtonColor,
    padding: 10,
    margin : 10,
    borderRadius: 5,
    justifyContent : "center",
    alignSelf : "center",
  },
  cancelButtonText : {
    alignItems : "center",
    color : "white",
    textAlign : "center",
    fontSize : 18 
  },
  saveButton : {
    width : "95%",
    backgroundColor: config.colors.confirmButtonColor,
    padding: 10,
    margin : 10,
    borderRadius: 5,
    justifyContent : "center",
    alignSelf : "center",
    
  },
  saveButtonText : {
    alignItems : "center",
    color : "white",
    textAlign : "center",
    fontSize : 18,
  },
})

const datePickerStyles = StyleSheet.create({
  datePickerDateIcon:{
    position: 'absolute',
    right: 0,
    top: 4,
    marginRight: 0,
    width : 30,
    height : 30,
  },
  datePickerDateInput : {
    marginLeft: 0,
    borderWidth : 0,
    alignItems : "flex-start"
  },
  datePickerDateText : {
    marginLeft: 5,
    fontSize : 20
  },
  datePickerPlaceholderText : {
    textAlign :"left",
    marginLeft: 5,
    color : config.colors.placeholderTextColor,
  },
});