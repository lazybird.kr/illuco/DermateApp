import React, {Component}  from "react";
import {View, Text, Button, StyleSheet, TouchableOpacity, ScrollView, FlatList, TextInput, ActivityIndicator, Alert} from "react-native";
import {BackIcon, MenuIcon, HeaderTitle} from 'src/components/header';
import config from 'src/config';
import DateTimePicker from 'react-native-datepicker';
import AsyncStorage from '@react-native-community/async-storage';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as search from 'src/modules/search';
import * as detailActions from 'src/modules/detail';
import {ConfirmDialog, ConfirmOkDialog, PatientInputModal} from "src/components/dialog";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Modal from "react-native-modal";
import { DermMultiSelect, DermSingleSelect, SelectTags } from "src/components/select";
import {saveFile, prepareImage, processImage, getFormatDate, addDiagnosis} from 'src/modules/utils'
import {NavigationActions, StackActions} from 'react-navigation';   
import {InputBox} from 'src/components/textInput';    
import {Appearance} from 'react-native-appearance'

const colorScheme = Appearance.getColorScheme();

class QuickSnapNewImage extends Component {

  constructor() {
    super();
    this.state = {

      isFocused : false,
      selectedPatient : null,
      loading : false,
      isConfirmVisible : false,
      isConfirmOkVisible : false,

      visitDate : getFormatDate(new Date(), '/'),
      multiTagModalOpen : false,
      signleTagModalOpen : false,
      
      selectedTags : [],
      selectedType : [],
      note : '',
      treatment : '',

      tagItems : [],
      typeItems : [],
      isInputBoxVisibleForTreatment : false,
      isInputBoxVisibleForNote : false,
      isPatientModalVisible : false,
    }
  }

  static navigationOptions = ({ navigation }) => {
    const image=config.images.imageIcon;
    return {
      headerLeft: <BackIcon navigation={navigation}/>,
      headerTitle : <HeaderTitle image={image} title="Image Infomation" />,
      headerRight : <MenuIcon navigation={navigation}/>,
      headerStyle : {
        backgroundColor: config.colors.headerColor,
      }
    }
  };

  
  componentDidUpdate(prevProps, prevState){
    
    const {isFocused, isConfirmOkVisible,selectedPatient,searchData} = this.state;
    console.log("QuickSnapNewImage componentiDidUpdate isFocusd : ", prevState.isFocused, isFocused);
    if(prevState.isFocused != isFocused && isFocused){
        AsyncStorage.getItem('access_info')
      .then(value => { 
        console.log("value, ", value);
        const access_info = JSON.parse(value);
        console.log("access_info : ", access_info);
        const { SearchActions, condition, searchKey  } = this.props;

        let order = searchKey[condition].order;
        let order_by = searchKey[condition].order_by;
        SearchActions.getMainList(access_info.access_token,
            condition, 
            order,
            order_by,
            null, 
            null,
            null,
            null,
            null,
            )
        });
    }
    
    if(prevProps.searchResult.patient_list != this.props.searchResult.patient_list){
      this.setState({tagItems : this.props.itemList.tag_item_list,
        typeItems : this.props.itemList.disease_item_list,
      });
    }

    if(isConfirmOkVisible && prevState.isConfirmOkVisible != isConfirmOkVisible){
      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'home'})
        ] });
      this.props.navigation.dispatch(resetAction);
    }
  }

  componentDidMount() {
    console.log("QuickSnapNewImage componentDidMount()");
    const {DetailActions, } = this.props;
    AsyncStorage.getItem('access_info')
    .then(value => { 
      const access_info = JSON.parse(value);
      this.setState({access_info : access_info});
      
        DetailActions.getItemData(access_info.access_token, 'ethnicity')
        DetailActions.getItemData(access_info.access_token, 'country')
        DetailActions.getItemData(access_info.access_token, 'skin_type')
        DetailActions.getItemData(access_info.access_token, 'tag')
        DetailActions.getItemData(access_info.access_token, 'disease')
    });
      this.subs = [
        this.props.navigation.addListener(
          "didFocus", () => {console.log("didFocus"); this.setState({isFocused: true});}),
        this.props.navigation.addListener("willBlur", () => {console.log("willBlur"); this.setState({ isFocused: false, });}),
        this.props.navigation.addListener("willFocus", () => console.log("willFocus")),
        this.props.navigation.addListener("didBlur", () => console.log("didBlur")),
      ];   
  }

  shouldComponentUpdate(nextProps){
    return true;
  }

  componentWillUnmount() {
    console.log("QuickSnapImage componentWillUnmount()");
    this.subs.forEach((sub) => {
      sub.remove();
    });
  }

  handleSelectPatient = (patient) => {
    this.setState({
      selectedPatient : patient,
    });
    this.togglePatient();
  }

  handleClearPatient = () => {
    this.setState({
      selectedPatient : null,
    });
  }

  multiToggleModal = () =>{
    this.setState({multiTagModalOpen : !this.state.multiTagModalOpen});
  }

  singleToggleModal = () =>{
    this.setState({singleTagModalOpen : !this.state.singleTagModalOpen});
  }

  onSelectedTagsChange = selectedTags => {
    console.log("onSelectedItemsChange : ", selectedTags);
    this.setState({ selectedTags : selectedTags });
  };

  onSelectedTypeChange = selectedType => {
    console.log("onSelectedTypeChange : ", selectedType);
    this.setState({ selectedType : selectedType});
  };

  onAddTagItem = tagItems => {
    console.log("onAddTagItem : ", tagItems);
    let newItems = [];
    tagItems.forEach((i) => {
      newItems.push(i.name);
    })
    this.setState({ tagItems : newItems });
  };

  onAddTypeItem = typeItems => {
    console.log("onAddTypeItem : ", typeItems);
    let newItems = [];
    typeItems.forEach((i) => {
      newItems.push(i.name);
    })
    this.setState({ typeItems : newItems });
  };

  importImage = () => {
    
    const {itemList, DetailActions} = this.props;
    this.setState({isConfirmVisible : false,
                  loading : true});
    const {access_info, selectedPatient, selectedTags, selectedType, visitDate} = this.state;

    let file = { 
      uri : this.props.navigation.state.params.cropImageData.path,
      name : new Date().getTime().toString() + ".jpg", 
      type: 'image/jpeg'
    };
    prepareImage(access_info.access_token, file).then((image_info) => {
      saveFile(image_info.uri.image, file).then(() => {
          processImage(access_info.access_token, image_info.image_id).then((resp) => {
              console.log('success!!', resp);
              /* 여기서 선택된 환자가 있다면 import 해야함 */
              if (selectedPatient){
                
                /*DetailActions.addItemData('disease', selectedType, itemList.disease_item_list)
                  DetailActions.addItemData('tag_list', selectedTags, itemList.tag_item_list);
                */
                
               let location_list = [];
               let image_list = [];
               location_list.push(this.props.navigation.state.params.location.three.mesh_list[0].location_name);
               image_list.push({"image_id" : image_info.image_id});
                const imageInfo ={
                  type : selectedType && selectedType[0] || '',
                  tag_list : selectedTags || [],
                  visit_date : visitDate,
                  mesh_list : this.props.navigation.state.params.location.three.mesh_list,
                  image_list : image_list,
                  patient_id : selectedPatient.patient_id,
                  location_list : location_list,
                  note : '',
                }
                console.log("IMAGE INFO : ", imageInfo)
                addDiagnosis(access_info.access_token, imageInfo)
                .then(resp => 
                  console.log(resp));
              }
              setTimeout(() => {
                this.setState({loading: false,
                              isConfirmOkVisible :  true})}, 1000)
          });
      })
    });   
  }

  importGalleyImage = () => {
    
    const {itemList, DetailActions} = this.props;
    this.setState({isConfirmVisible : false,
                  loading : true});
    const {access_info, note, selectedPatient, selectedTags, selectedType, visitDate} = this.state;
    if(!selectedPatient){
      alert("Patient Must be selected");
      return;
    }

    if (selectedPatient){
      /*
      DetailActions.addItemData('disease', selectedType, itemList.disease_item_list)
      DetailActions.addItemData('tag_list', selectedTags, itemList.tag_item_list);
      */
      
      let location_list = [];
      let image_list = [];
      
      this.props.navigation.state.params.selectedImageList.map(item =>{
        image_list.push({image_id : item.image_id})
      })
      const imageInfo ={
        type : selectedType && selectedType[0]|| '',
        tag_list : selectedTags || [],
        visit_date : visitDate,        
        image_list : image_list,
        patient_id : selectedPatient.patient_id,
        location_list : location_list,
        note : note,
      }
      console.log("IMAGE INFO grom Gallery : ", imageInfo);

      addDiagnosis(access_info.access_token, imageInfo)
      .then(resp => 
        console.log(resp));
      setTimeout(() => {
        this.setState({loading: false,
                      isConfirmOkVisible :  true})}, 3000)
    }
            
  }

  handleClickConfirmButton = () => {
    this.setState({isConfirmVisible : true});
  }

  handleClickNoOnConfirmDialog =  () => {
    this.setState({isConfirmVisible : false});
  }

  confirmOk = () => {
    this.setState({isConfirmOkVisible : false});
    /*
    이거 모달 안닫히고 리셋되는 문제 수정 필요 
    const resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'home'})
      ] });
    this.props.navigation.dispatch(resetAction);
      
   this.props.navigation.navigate("home");
   */
  }

  toggleInputBox = (type) => {
    if(type ==="treatment")
      this.setState({isInputBoxVisibleForTreatment : !this.state.isInputBoxVisibleForTreatment});
    else if(type === "note")
      this.setState({isInputBoxVisibleForNote : !this.state.isInputBoxVisibleForNote});
  }

  togglePatient = () => {
    this.setState({isPatientModalVisible : !this.state.isPatientModalVisible});
  }

  onChangeForTreatment = (text) => {
    this.setState({treatment : text});
  }
  onChangeForNote = (text) => {
    this.setState({note : text});
  }
  onChangeForVisitDate = (date) => {
    this.setState({visitDate : date});
  }

  render() {
    const { isConfirmOkVisible, isInputBoxVisibleForNote, 
        isInputBoxVisibleForTreatment, multiTagModalOpen, isPatientModalVisible,
        isConfirmVisible, loading, note, treatment,
        selectedPatient, singleTagModalOpen, visitDate, 
        tagItems, typeItems, selectedTags, selectedType
    } = this.state;
    
    const { onAddTagItem, onSelectedTagsChange, handleClickConfirmButton,
            handleClickNoOnConfirmDialog, onChangeForVisitDate, onChangeForNote,
            handleSelectPatient, toggleInputBox, onChangeForTreatment, togglePatient,
            multiToggleModal, onSelectedTypeChange, onAddTypeItem, singleToggleModal
    } = this;

    const {searchResult} = this.props;
    const location = (this.props.navigation.state.params && this.props.navigation.state.params.location) ? 
        this.props.navigation.state.params.location.three.mesh_list[0].location_name : '';
       
    const contentTextOnConfirm = selectedPatient ? 
      "Import an image for the patient you selected" : 
      "The Image will be imported to the library. You didn't select a patient.";

    const whereFrom = this.props.navigation.state.params && this.props.navigation.state.params.whereFrom ?
      this.props.navigation.state.params.whereFrom : '';

    console.log("QuickSnapNewImage render()");

    return (
      <View style={styles.container}>
        {/* Modal and Dialog */}
        <Modal isVisible={isInputBoxVisibleForTreatment}
              animationType={'none'}
              onBackdropPress={()=>toggleInputBox("treatment")}
              style={styles.modalStyle}>
          <InputBox onChangeText={onChangeForTreatment} 
              value={treatment} onSubmitEditing={()=>toggleInputBox("treatment")}/>
        </Modal>
        <Modal isVisible={isInputBoxVisibleForNote}
              animationType={'none'}
              onBackdropPress={()=>toggleInputBox("note")}
              style={styles.modalStyle}>
          <InputBox onChangeText={onChangeForNote} 
              value={note} onSubmitEditing={()=>toggleInputBox("note")}/>
        </Modal>
        <Modal isVisible={loading}
              animationType={'none'}>
          <View style={styles.loading}>
            <ActivityIndicator size='large' />
          </View>
        </Modal>
        <Modal isVisible={multiTagModalOpen}
              onBackdropPress={multiToggleModal}
              avoidKeyboard = {true}
              style={styles.modalStyle} >
          <DermMultiSelect items={tagItems} selectedItems={selectedTags}
            style={{flex:1}} onSelectedItemsChange={onSelectedTagsChange}
            onAddItem={onAddTagItem} onCloseList={multiToggleModal}
            title="Tag" />
        </Modal>
        <Modal isVisible={singleTagModalOpen}
              onBackdropPress={singleToggleModal}
              avoidKeyboard={true}
              style={styles.modalStyle} >
          <DermSingleSelect items={typeItems} selectedItems={selectedType}
            style={{flex:1}}
            onSelectedItemsChange={onSelectedTypeChange}
            onAddItem={onAddTypeItem}
            onToggleList={singleToggleModal}
            onCloseList={singleToggleModal}
            title="Disease Type" /> 
        </Modal>
        <Modal isVisible={isPatientModalVisible}
              onBackdropPress={togglePatient}
              avoidKeyboard={true}
              style={styles.modalStyle} >
          <PatientInputModal
            togglePatient={togglePatient}
            searchResult={searchResult}
            handleSelectPatient={handleSelectPatient} />
        </Modal>
        <ConfirmDialog 
              titleText="Import Image" 
              contentText={contentTextOnConfirm}
              clickNoButton={handleClickNoOnConfirmDialog}
              clickYesButton={whereFrom === "Gallery" ? 
                  this.importGalleyImage : this.importImage}
              isVisable={isConfirmVisible} />
        {/* body */}
        <View style={styles.body}>
          <View style={{...styles.bodyItem1, ...{marginBottom: 10}}}>
            <View style={{flex: 3, justifyContent:"center"}}>
              <Text style={{...styles.itemTextStyle, ...{paddingLeft: 10, }}}>Patient</Text>
            </View>
            <View style={{flex:6, justifyContent:"center"}}>
              <Text pointerEvents="none" editable={false} style={{...styles.itemTextStyle, ...{paddingLeft: 5, color : selectedPatient? "black" : "grey"}}}>
                {selectedPatient? selectedPatient.name: 'Choose'}</Text>
            </View>
            <View style={{flex:1, justifyContent:"center"}}>
              <Icon onPress={this.togglePatient} name="chevron-down" size={28} color="grey"/>
            </View>
          </View>
          <View style={{...styles.bodyItem1, ...{marginBottom: 10}}} >
            <View style={{flex: 3, justifyContent:"center"}}>
              <Text style={{...styles.itemTextStyle, ...{paddingLeft: 10}}}>Visit Date</Text>
            </View>
            <DateTimePicker
                      style={{...styles.itemTextInput, ...{flex:7, marginLeft:0, justifyContent:"center"}}}
                      date={visitDate} mode="date" androidMode="default"
                      format="YYYY/MM/DD" minDate="1910/01/01"
                      confirmBtnText="Confirm" cancelBtnText="Cancel"
                      customStyles={{
                        datePicker: {backgroundColor:colorScheme === "dark" ? "#222" : "white" },
                        datePickerCon: { backgroundColor: colorScheme === 'dark' ? '#333' : 'white'},
                        dateIcon: datePickerStyles.datePickerDateIcon,
                        dateInput: datePickerStyles.datePickerDateInput,
                        dateText: datePickerStyles.datePickerDateText,
                        placeholderText: datePickerStyles.datePickerPlaceholderText,
                      }}
                      onDateChange={onChangeForVisitDate}
                    />
          </View>
          <View style={{flex:0.5, marginBottom: 10, backgroundColor : "#e0f4f7", justifyContent:"center" }}>
            <Text style={styles.itemTextStyle}>Disease</Text>
          </View>
          <View style={{...styles.bodyItem1, ...{borderBottomWidth: 0}}}>
            <View style={{flex: 3, justifyContent:"center"}}>
              <Text style={{...styles.itemTextStyle, ...{paddingLeft: 10}}}>Type</Text>
            </View>
            <View style={{flex:6, justifyContent:"center"}}>
              <Text style={{...styles.itemTextStyle, ...{paddingLeft: 5, color : selectedType && selectedType[0] ? "black" : "grey" }}}>
              {selectedType && selectedType[0]? selectedType[0] : "Choose"}</Text>
            </View>
            <View style={{flex:1, justifyContent:"center"}}>
              <Icon onPress={singleToggleModal} name="chevron-down" size={28} color="grey"/>
            </View>
          </View>
          <View style={{...styles.bodyItem1, ...{borderBottomWidth: 0}}}>
            <View style={{flex: 3, justifyContent:"center"}}>
              <Text style={{...styles.itemTextStyle, ...{paddingLeft: 10}}}>Location</Text>
            </View>
            <View  style={{flex:6, justifyContent:"center"}}>
              <Text style={{...styles.itemTextStyle, ...{paddingLeft: 5, color : location && location.length ? "black" : "grey"}}} >
                {location && location.length? location : "Choose"}
              </Text>
            </View>
            <View style={{flex:1, justifyContent:"center"}}>
              <Icon onPress={()=>{}} name="chevron-down" size={28} color="grey"/>
            </View>
          </View>

          <View style={{...styles.bodyItem2, ...{marginBottom: 10}}}>
            <View style={{flex:1, justifyContent: "center",}}>
              <Text style={{...styles.itemTextStyle, ...{paddingLeft: 10}}}>Treatment</Text>
            </View>
            <View style={{flex: 2, flexDirection: "row"}}>
              <View style={{flex:9}}>
                <ScrollView>
                  <Text  pointerEvents="none" style={styles.itemTextInput}>{treatment}</Text>
                </ScrollView>
              </View>
              <View style={{flex: 1, justifyContent: "center", }}>
                <Icon onPress={()=>toggleInputBox("treatment")} name="chevron-down" size={28} color="grey"/>
            </View>
            </View>
         </View>

         <View style={{...styles.bodyItem2, ...{marginBottom: 10}}}>
            <View style={{flex:1, justifyContent: "center",}}>
              <Text style={{...styles.itemTextStyle, ...{paddingLeft: 10}}}>Note</Text>
            </View>
            <View style={{flex: 2, flexDirection: "row"}}>
              <View style={{flex:9}}>
                <ScrollView>
                  <Text  pointerEvents="none" style={styles.itemTextInput}>{note}</Text>
                </ScrollView>
              </View>
              <View style={{flex: 1, justifyContent: "center", }}>
                <Icon onPress={()=>toggleInputBox("note")} name="chevron-down" size={28} color="grey"/>
            </View>
            </View>
         </View>
         
         <View style={{...styles.bodyItem1, ...{flex:2}}}>
            <View style={{flex:3, justifyContent:"center",}}>
              <Text style={{...styles.itemTextStyle, ...{paddingLeft: 10}}}>Tag</Text>
            </View>
            <View style={{flex:6, justifyContent:"center"}}>
                {selectedTags && selectedTags.length > 0 ? 
                <ScrollView>
                  <SelectTags selectedItems={selectedTags} items={tagItems} 
                    onSelectedItemsChange={onSelectedTagsChange}
                    onAddItem={onAddTagItem} />
                  </ScrollView> :
                <Text style={{...styles.itemTextStyle, ...{paddingLeft: 5,color : "grey"}}}>Choose</Text>}
            </View>
            <View style={{flex:1, justifyContent:"center", }}>
                <Icon onPress={multiToggleModal} name="chevron-down" size={28} color="grey"/>
            </View>
         </View>
        </View> 
        {/* end of body */}
        <View style={styles.bottom}>
          <TouchableOpacity style={styles.confirmButton} onPress={handleClickConfirmButton}>
            <Text style={styles.confirmButtonText}>Confirm</Text>
          </TouchableOpacity>
        </View>
      </View> 
    );
  }
}

const styles = StyleSheet.create({
  container : {
    flex: 1,
    height: "100%",
  },
  body:{
    flex: 8,
    margin: 10,
    flexDirection : "column",
    paddingLeft: 10, paddingRight: 10,
  },
  bottom : {
    flex : 1,
    backgroundColor: config.colors.confirmBackGroundColor,
    justifyContent: "center",
    alignItems: "center",
  },
  confirmButton : {
    width: "95%",
    backgroundColor: config.colors.confirmButtonColor,
    padding: 10, margin: 10,
    borderRadius: 5,    
  },
  confirmButtonText : {
    alignItems: "center",
    color: "white",
    textAlign: "center",
    fontSize: 18,
  },
  modalStyle: { 
    flex: 1,
    justifyContent: "flex-end",
    margin: 0
  },
  bodyItem1 : {
    flex: 1, borderColor: "grey", borderWidth: 1, flexDirection: "row"
  },
  bodyItem2 : {
    flex: 3, borderColor: "grey", borderWidth: 1, flexDirection: "column"
  },
  itemTextInput : {
    marginLeft: 15,
  },
  itemTextStyle : {
    fontSize: 16,
    color: "#354d67"
  },
  loading: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
})

const datePickerStyles = StyleSheet.create({
  datePickerDateIcon:{
    position: 'absolute',
    right: 0, top: 4,
    marginRight: 0,
    width : 30, height : 30,
    justifyContent:"center"
  },
  datePickerDateInput : {
    marginLeft: 0,
    borderWidth : 0,
    alignItems : "flex-start",
  },
  datePickerDateText : {
    marginLeft: 5, fontSize : 16,
    textAlign : "left",
    justifyContent:"center",
  },
  datePickerPlaceholderText : {
    marginLeft: 5,
    textAlign :"left",
  },
});

export default connect(
  (state) => ({
      itemList : state.detail.get('itemList').toJS(),
      searchStatus: state.search.get('searchStatus'),
      searchKey: state.search.get('searchKey').toJS(),
      condition: state.search.get('searchCondition'),
      searchResult: state.search.get('searchResult').toJS(),
      selectedRow: state.search.get('selectedRow').toJS(),      
  }),
  (dispatch) => ({
      DetailActions:bindActionCreators(detailActions, dispatch),
      SearchActions: bindActionCreators(search, dispatch),
  })
)(QuickSnapNewImage);
